package com.snails.logger;


import android.text.TextUtils;


public class LoggerConfig {
    // 调试标签
    private String tag = "Snails";

    // 调试输出，true-调试状态输出日志，false-非调试状态，不输出日志
    private boolean debug = false;

    // 线程信息输出，true-输出，false-不输出
    private boolean showThreadInfo = true;

    // 打开日志输出时的线程休眠【保证打印信息对齐性】
    private boolean debug_logger_thread_sleep = true;

    // -----------------------------------------------------------------
    // set
    // -----------------------------------------------------------------
    /**
     * 每个日志的全局标记，默认-'Snails'
     */
    public LoggerConfig setTag(String tag) {
        if (!TextUtils.isEmpty(tag)) {
            this.tag = tag;
        }
        return this;
    }

    /**
     * 是否打印日志，默认false-不打印
     */
    public LoggerConfig setDebug(boolean debug) {
        this.debug = debug;
        return this;
    }

    /**
     * 是否显示线程信息。默认true-显示
     */
    public LoggerConfig setThreadInfo(boolean show) {
        this.showThreadInfo = show;
        return this;
    }

    /**
     * 控制台打印日志是否格式化，默认true-格式化
     * 【保证打印信息对齐性，没打印一次线程会进行短暂的休眠】
     */
    public LoggerConfig setFormat(boolean format) {
        this.debug_logger_thread_sleep = format;
        return this;
    }

    // -----------------------------------------------------------------
    // get
    // -----------------------------------------------------------------
    public String tag() {
        return tag;
    }

    public boolean debug() {
        return debug;
    }

    public boolean threadInfo() {
        return showThreadInfo;
    }

    public boolean format() {
        return debug_logger_thread_sleep;
    }
}

package com.snails.logger;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IPrinter {

    LoggerConfig init();

    boolean debug();

    boolean format();

    void setThreadInfo(boolean show);

    boolean getThreadInfo();

    String getFormatLog();

    void d(String message, Object... args);

    void e(String message, Object... args);

    void e(Throwable throwable, String message, Object... args);

    void w(String message, Object... args);

    void i(String message, Object... args);

    void v(String message, Object... args);

    void wtf(String message, Object... args);

    void json(String json);

    void xml(String xml);

    void map(Map map);

    void list(List list);

    void set(Set set);
}

# Logger

#### 介绍
Logger — 日志打印工具

#### 软件架构
无


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

*    **Logger  日志打印工具**

1.   **使用示例**

```

Logger.d("打印一句话.");

```

2.   **打印日志配置**

* LoggerConfig

```

setTag(String tag): LoggerConfig                每个日志的全局标记，默认-'Snails'
setDebug(boolean debug): LoggerConfig           是否打印日志，默认false-不打印
setThreadInfo(boolean show): LoggerConfig       是否显示线程信息。默认true-显示
setFormat(boolean format): LoggerConfig         控制台打印日志是否格式化，默认true-格式化【保证打印信息对齐性，没打印一次线程会进行短暂的休眠】

tag(): String               得到日志的全局标记
debug(): boolean            得到是否打印日志
threadInfo(): boolean       得到是否显示线程信息
format(): boolean           得到是否格式化控制台打印日志

```

3.   **打印日志方法**

* Logger

```

debug(): boolean            得到是否打印日志
init(): LoggerConfig        得到配置文件，设置相应的配置
getFormatLog(): String      返回最后一次格式化的打印结果样式


d(String message, Object... args): void
e(String message, Object... args): void
e(Throwable throwable, String message, Object... args): void
i(String message, Object... args): void
v(String message, Object... args): void
w(String message, Object... args): void
wtf(String message, Object... args): void


// 支出字符串格式化
Logger.d("hello %s", "world");

// 支持集合（仅适用于调试日志)
Logger.map(MAP);
Logger.list(LIST);
Logger.set(SET);

// Json和Xml支持（输出将处于调试级别）
Logger.json(JSON_CONTENT);
Logger.xml(XML_CONTENT);

```


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
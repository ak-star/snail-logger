package com.snails.logger;

import java.util.List;
import java.util.Map;
import java.util.Set;

public final class Logger {

    private static final IPrinter I_PRINTER = new LoggerPrinter();

    private Logger() {
    }

    public static boolean debug() {
        return I_PRINTER != null ? I_PRINTER.debug() : false;
    }

    public static boolean format() {
        return I_PRINTER != null ? I_PRINTER.format() : false;
    }

    /**
     * 是否显示线程信息。默认true-显示
     */
    public static void setThreadInfo(boolean show) {
        if (I_PRINTER != null) {
            I_PRINTER.setThreadInfo(show);
        }
    }

    public static boolean getThreadInfo() {
        return I_PRINTER != null ? I_PRINTER.getThreadInfo() : false;
    }

    public static LoggerConfig init() {
        return I_PRINTER.init();
    }

    public static String getFormatLog() {
        return I_PRINTER.getFormatLog();
    }

    public static void d(String message, Object... args) {
        I_PRINTER.d(message, args);
    }

    public static void e(String message, Object... args) {
        I_PRINTER.e(null, message, args);
    }

    public static void e(Throwable throwable, String message, Object... args) {
        I_PRINTER.e(throwable, message, args);
    }

    public static void i(String message, Object... args) {
        I_PRINTER.i(message, args);
    }

    public static void v(String message, Object... args) {
        I_PRINTER.v(message, args);
    }

    public static void w(String message, Object... args) {
        I_PRINTER.w(message, args);
    }

    public static void wtf(String message, Object... args) {
        I_PRINTER.wtf(message, args);
    }

    public static void json(String json) {
        I_PRINTER.json(json);
    }

    public static void xml(String xml) {
        I_PRINTER.xml(xml);
    }

    public static void map(Map map) {
        I_PRINTER.map(map);
    }

    public static void list(List list) {
        I_PRINTER.list(list);
    }

    public static void set(Set set) {
        I_PRINTER.set(set);
    }

}
